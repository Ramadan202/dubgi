package com.dubci.dubgi.notify;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

/**
 * Created by CodeSlu on 9/4/2018.
 */

public interface FCM {
    @Headers({

            "Content-Type: application/json",
            "Authorization:key=AAAAWH5bN1M:APA91bEy4_ceL6XJZJ-B-3qOSCBKVqr8PWTrrA235AEF0DZQQoYQghvxmiquJKXA8z00mEIpMrXbQupT8c2bugy5FM9PoFAag7_LQGaRitQZoUcyMwAiT1sDoeLrYsAadnCS0C06D1fn"
    })
    @POST("fcm/send")
    Call<FCMresp> send(@Body Sender body);
}
