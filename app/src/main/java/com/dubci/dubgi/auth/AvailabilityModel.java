package com.dubci.dubgi.auth;

public class AvailabilityModel {
    private Integer Key;
    private String Message;
    private String Title;

    public AvailabilityModel() {
    }

    public AvailabilityModel(Integer key, String message, String title) {
        Key = key;
        Message = message;
        Title = title;
    }

    public Integer getKey() {
        return Key;
    }

    public void setKey(Integer key) {
        Key = key;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }
}
