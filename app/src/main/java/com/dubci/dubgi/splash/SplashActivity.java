package com.dubci.dubgi.splash;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import com.dubci.dubgi.MainActivity;
import com.dubci.dubgi.R;
import com.dubci.dubgi.auth.AvailabilityModel;
import com.dubci.dubgi.auth.Login;
import com.dubci.dubgi.auth.Verify;
import com.dubci.dubgi.global.Global;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Objects;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;

public class SplashActivity extends AppCompatActivity {
    private int  SPLASH_DISPLAY_LENGTH = 4000;
    private DatabaseReference availabilityPath;
    AlertDialog.Builder builder;
    private int isExist =1;
    private  AlertDialog alert;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        availabilityPath= FirebaseDatabase.getInstance().getReference(Global.AVAILABILITY);
        builder = new AlertDialog.Builder(this);
        Observable.interval(1, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext(x -> {
                    authing();
                })
                .takeUntil(aLong -> aLong == SPLASH_DISPLAY_LENGTH)
                .doOnComplete(() -> {
                    if(isExist==1) {
                        Intent mainIntent = new Intent(this, MainActivity.class);
                        startActivity(mainIntent);
                        finish();
                    }
                    else if(isExist==2)
                    {
                        if(alert!=null)
                        {
                            alert.show();
                        }
                    }
                }).subscribe();
    }

    public void authing() {
        availabilityPath.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    AvailabilityModel availabilityModel = dataSnapshot.getValue(AvailabilityModel.class);
                    if (availabilityModel != null) {
                        if (Objects.requireNonNull(availabilityModel).getKey() == 0) {
                            isExist=1;
                        } else if (Objects.requireNonNull(availabilityModel).getKey() == 1) {
                            {
                                isExist=2;
                                //Uncomment the below code to Set the message and title from the strings.xml file
                                builder.setMessage(availabilityModel.getMessage()) .setTitle(availabilityModel.getTitle());
                                builder.setIcon(android.R.drawable.ic_dialog_alert);

                                //Setting message manually and performing action on button click
                                builder
                                        .setCancelable(false)
                                        .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                                finish();
                                            }
                                        })
                                ;
                                //Creating dialog box
                                 alert = builder.create();
                                //Setting the title manually

                            }
                        }
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


    }
}