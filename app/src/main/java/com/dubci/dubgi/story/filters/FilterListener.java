package com.dubci.dubgi.story.filters;

import ja.burhanrashid52.photoeditor.PhotoFilter;

public interface FilterListener {
    void onFilterSelected(PhotoFilter photoFilter);
}